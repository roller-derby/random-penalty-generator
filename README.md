# Random Penalty Generator

A tool to practice calling roller derby penalties.

## How do I use this ?

- Locally, open `index.html` in your browser, or go to [the online webpage](https://roller-derby.frama.io/-/random-penalty-generator/-/jobs/2092210/artifacts/public/index.html).
- Say the call and do the hand gesture for the displayed penalty (for instance "Orange, seven two, back block").
- If you don't know what the proper hand signal is for that penalty, you can scroll down to the bottom of the page for a hint.
- Reload the page (or click on the jersey) and repeat.

## License

This work is shared with a CC-BY-SA 4.0 license. 

## Disclaimer

This tool is shared with the hope that it may be useful, but it may or may not work. There might be errors. The author has no affiliation with the WFTDA. 
